<?php

namespace Instilla\Bundle\MakerBundle;

use Instilla\Bundle\MakerBundle\DependencyInjection\InstillaMakerExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/*
 * Purpose of this bundle is to extend the MakerBundle with a custom entity structure
 *
 * Author: Luca Nardelli <luca.nardelli@instilla.it>
 */

class InstillaMakerBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
    }

    public function getContainerExtension()
    {
        return new InstillaMakerExtension();
    }
}

