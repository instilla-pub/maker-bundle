<?php
/**
 * Created by PhpStorm.
 * User: luca
 * Date: 19/02/19
 * Time: 11.43
 */

namespace Instilla\Bundle\MakerBundle\Maker;


use PhpParser\Builder\Method;
use PhpParser\BuilderFactory;
use PhpParser\Comment\Doc;
use PhpParser\Lexer;
use PhpParser\Node;
use PhpParser\Node\Stmt\Class_;
use PhpParser\NodeFinder;
use PhpParser\Parser;
use Symfony\Bundle\MakerBundle\ConsoleStyle;
use Symfony\Bundle\MakerBundle\DependencyBuilder;
use Symfony\Bundle\MakerBundle\Generator;
use Symfony\Bundle\MakerBundle\InputAwareMakerInterface;
use Symfony\Bundle\MakerBundle\InputConfiguration;
use Symfony\Bundle\MakerBundle\Maker\AbstractMaker;
use Symfony\Bundle\MakerBundle\Maker\MakeEntity;
use Symfony\Bundle\MakerBundle\Util\ClassDetails;
use Symfony\Bundle\MakerBundle\Util\PrettyPrinter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;

class MakeInstillaEntity extends AbstractMaker implements InputAwareMakerInterface
{
    private $sourceCode;
    private $oldStmts;
    private $oldTokens;
    private $newStmts;

    private $printer;
    private $lexer;
    private $parser;

    private $em;

    public function __construct(MakeEntity $em)
    {
        $this->em      = $em;
        $this->lexer   = new Lexer\Emulative([
            'usedAttributes' => [
                'comments',
                'startLine', 'endLine',
                'startTokenPos', 'endTokenPos',
            ],
        ]);
        $this->parser  = new Parser\Php7($this->lexer);
        $this->printer = new PrettyPrinter();
    }

    /**
     * Return the command name for your maker (e.g. make:report).
     *
     * @return string
     */
    public static function getCommandName(): string
    {
        return "make:instilla-entity";
    }

    /**
     * Configure the command: set description, input arguments, options, etc.
     *
     * By default, all arguments will be asked interactively. If you want
     * to avoid that, use the $inputConfig->setArgumentAsNonInteractive() method.
     *
     * @param Command $command
     * @param InputConfiguration $inputConfig
     */
    public function configureCommand(Command $command, InputConfiguration $inputConfig)
    {
        $this->em->configureCommand($command, $inputConfig);
    }

    /**
     * Called after normal code generation: allows you to do anything.
     *
     * @param InputInterface $input
     * @param ConsoleStyle $io
     * @param Generator $generator
     * @throws \Exception
     */
    public function generate(InputInterface $input, ConsoleStyle $io, Generator $generator)
    {
        $name               = $input->getArgument('name');
        $entityClassDetails = $generator->createClassNameDetails(
            $name,
            'Entity\\'
        );
        $classExists        = class_exists($entityClassDetails->getFullName());

        $this->em->generate($input, $io, $generator);

        // Overrides
        if ($classExists) {
            // Do nothing as we don't have anything to do here
            return;
        }
        $entityPath = $this->getPathOfClass($entityClassDetails->getFullName());

        $classSource = file_get_contents($entityPath);

        $ast = $this->parser->parse($classSource);

        $finder = new NodeFinder();

        // Add use statement
        $this->addUseStatement($ast, ["Ramsey", "Uuid", "Uuid"]);

        /** @var Class_ $class */
        $class = $finder->findFirst($ast, function (Node $node) use ($name) {
            return $node instanceof Node\Stmt\Class_
                && $node->name->toString() === $name;
        });

        // Add class annotations
        $annotations = $class->getDocComment();
        $txt         = $annotations->getText();
        $txt         = rtrim($txt, "*/");
        $txt         = $txt . "* @ORM\HasLifecycleCallbacks()\n */";
        $class->setDocComment(new Doc($txt, $annotations->getLine(), $annotations->getFilePos(), $annotations->getTokenPos()));

        $bf = new BuilderFactory();

        // Add updatedAt and createdAt properties, and getters

        $class->stmts[] = $this->createDatetimeProperty('createdAt');
        $class->stmts[] = $this->createGetter('getCreatedAt','this->createdAt');
        $class->stmts[] = $this->createGetter('getUpdatedAt','this->updatedAt');
        $class->stmts[] = $this->createDatetimeProperty('updatedAt');

        // Add preInsert and preUpdate
        $builder = new Method("prePersist");
        $builder->makePublic()->addStmt(
            new Node\Stmt\Expression(
                new Node\Expr\Assign(
                    new Node\Expr\Variable('this->createdAt'),
                    $bf->new("\DateTime", ['now'])
                )));
        $builder->addStmt(
            new Node\Stmt\Expression(
                new Node\Expr\Assign(
                    new Node\Expr\Variable('this->updatedAt'),
                    $bf->new("\DateTime", ['now'])
                )));
        $node = $builder->getNode();
        $str  = <<<EOD
/**
 * @ORM\PrePersist()
 */
EOD;
        $node->setDocComment(new Doc($str));
        $class->stmts[] = $node;

        $builder = new Method("preUpdate");
        $builder->makePublic()->addStmt(
            new Node\Stmt\Expression(
                new Node\Expr\Assign(
                    new Node\Expr\Variable('this->updatedAt'),
                    $bf->new("\DateTime", ['now'])
                )));
        $node = $builder->getNode();
        $str  = <<<EOD
/**
 * @ORM\PreUpdate()
 */
EOD;
        $node->setDocComment(new Doc($str));
        $class->stmts[] = $node;


        //Update id field
        $propId = $finder->findFirst($ast, function (Node $node) use ($name) {
            return $node instanceof Node\Stmt\Property
                && $node->props[0]->name->name === "id";
        });
        $str    = <<<EOD
/**
 * @ORM\Id()
 * @ORM\Column(type="guid")
 */
EOD;
        $propId->setDocComment(new Doc($str));


        // Update getId
        /** @var Node\Stmt\ClassMethod $node */
        $node = $finder->findFirst($ast, function (Node $node) use ($name) {
            return $node instanceof Node\Stmt\ClassMethod
                && $node->name->toString() === "getId";
        });
        if (!$node) {
            throw new \RuntimeException("Could not find getId");
        }
        $node->returnType = new Node\NullableType(new Node\Identifier("string"));

        // Fix constructor
        $node = $finder->findFirst($ast, function (Node $node) use ($name) {
            return $node instanceof Node\Stmt\ClassMethod
                && $node->name->toString() === "__construct";
        });
        if (!$node) {
            $node = $bf->method("__construct")->makePublic()->getNode();
        }
        $node->stmts[]  = new Node\Stmt\Expression(
            new Node\Expr\Assign(
                new Node\Expr\Variable('this->id'),
                new Node\Expr\MethodCall(
                    new Node\Expr\StaticCall(
                        new Node\Name("Uuid"),
                        new Node\Name("uuid4")
                    ),
                    new Node\Name('toString')
                )
            ));
        $class->stmts[] = $node;


        $code = $this->printer->prettyPrint($ast);

        // Dump to file
        file_put_contents($entityPath, "<?php\n\n$code");
    }

    private function createGetter(string $getterName, string $property){
        $method = new Method($getterName);
        $method->makePublic()->addStmt(
            new Node\Stmt\Return_(
                new Node\Expr\Variable($property)
            )
        );
        return $method->getNode();
    }

    private function createDatetimeProperty(string $propertyName){
        $bf = new BuilderFactory();
        $element = $bf->property($propertyName)->makePrivate();
        $element = $element->getNode();
        $element->setDocComment(new Doc(<<<EOD
/**
 * @ORM\Column(type="datetime")
 */
EOD
        ));
        return $element;
    }

    private function getPathOfClass(string $class): string
    {
        $classDetails = new ClassDetails($class);

        return $classDetails->getPath();
    }

    private function addUseStatement(array $nodes, array $parts)
    {
        // Add use statement
        array_unshift($nodes[0]->stmts,
            new Node\Stmt\Use_([
                new Node\Stmt\UseUse(new Node\Name($parts))
            ]));
    }

    /**
     * Configure any library dependencies that your maker requires.
     *
     * @param DependencyBuilder $dependencies
     * @param InputInterface|null $input
     */
    public function configureDependencies(DependencyBuilder $dependencies, InputInterface $input = null)
    {
        $this->em->configureDependencies($dependencies, $input);
    }

    public function interact(InputInterface $input, ConsoleStyle $io, Command $command)
    {
        $this->em->interact($input, $io, $command);
    }
}
